/**
 * 
 * @author jacob_000
 *create an interface, prgram in general, create abstract methods,and concrete classes
 *
 *abstract 
 *-cannot be initialized
 *anything abstract mustt be implemented as concrete 
 *implement interface, can add as many as you want
 */
public abstract class ACar implements ICar{
	/** make of car */
	private String make;
	
	private String model;
	
	private int year;
	
	private int mileage;
	
	public ACar(String inMake, String inModel, int inYear) {
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}
	@Override
	public String toString(){
		return getMake() + " " + getModel();
	}

	
	
	
}
