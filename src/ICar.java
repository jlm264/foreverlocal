/**
 * 
 * @author jacob_000
 *class : combination of data and methods
 *enum
 *primitive
 *interface : only defines methods that subclasses need
 and by def. all are public */
public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
	
	
}
